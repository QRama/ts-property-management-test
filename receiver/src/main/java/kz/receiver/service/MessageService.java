package kz.receiver.service;

import kz.receiver.model.Message;
import kz.receiver.model.dto.MessageDto;
import kz.receiver.repository.MessageRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class MessageService {

    private final MessageRepository messageRepository;
    private final KafkaTemplate<String, MessageDto> kafkaTemplate;

    public void sendMessageToKafka(MessageDto message) {
        kafkaTemplate.send("message.send", message);
    }

    public List<Message> getLast10Messages() {
        return messageRepository.findTop10ByOrderByCreatedAtDesc();
    }

    public List<Message> getMessagesBySender(String sender) {
        return messageRepository.findBySender(sender);
    }

}
