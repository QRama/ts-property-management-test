package kz.sender.service;

import kz.sender.mapper.MessageMapper;
import kz.sender.model.Message;
import kz.sender.model.dto.MessageDto;
import kz.sender.model.dto.SentMessageDto;
import kz.sender.repository.MessageRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class MessageService {

    private final MessageRepository messageRepository;

    public void saveMessage(MessageDto messageDto, String responseCode) {
        Message message = Message.builder()
                .sender(messageDto.getSender())
                .content(messageDto.getContent())
                .responseCode(responseCode)
                .build();

        messageRepository.save(message);
    }

    public List<SentMessageDto> getSentMessages() {
        return messageRepository.findAll()
                .stream()
                .map(MessageMapper::toSentMessageDto)
                .collect(Collectors.toList());
    }

}
