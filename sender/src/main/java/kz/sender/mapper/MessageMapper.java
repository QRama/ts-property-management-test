package kz.sender.mapper;

import kz.sender.model.Message;
import kz.sender.model.dto.SentMessageDto;

public class MessageMapper {

    public static SentMessageDto toSentMessageDto(Message message) {
        SentMessageDto dto = new SentMessageDto();
        dto.setSender(message.getSender());
        dto.setContent(message.getContent());
        dto.setResponseCode(message.getResponseCode());
        return dto;
    }
}